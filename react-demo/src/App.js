import React, { Component } from 'react';
import './App.css';

import Demo from './Demo';

class App extends Component {
  render() {
    return (
      <Demo test="Demo"></Demo>
    );
  }
}

export default App;
