import React, { Component } from 'react';
import './Demo.css';

class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = { title: 'World' };
    
    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({title: 'Frontend Freunde'});
  }

  render() {
    return (
      <div>
        <h1 className="head">
          Hello {this.state.title}!
        </h1>
        <div>
          This is a {this.props.test}!
        </div>
        <button onClick={this.handleClick}>Click</button>
      </div>
    );
  }
}

export default Demo;
