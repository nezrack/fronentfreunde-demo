import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-demo',
  template: `
    <h1 class="title">
      Hello {{title}}!
    </h1>
    <div>
      This is a {{test}}!
    </div>
    <button (click)="handleClick()">Click</button>
  `,
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  @Input() test: string;
  title: string;

  ngOnInit() {
    this.title = 'World';
  }

  handleClick() {
    this.title = 'Frontend Freunde';
  }
}
